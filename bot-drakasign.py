from datetime import datetime, timedelta
from docusign_esign import EnvelopesApi, ApiClient
#from docusign import create_api_client
from docusign_esign.client.configuration import Configuration

#from app.docusign.utils import create_api_client
import requests
import urllib
import urllib3
urllib3.disable_warnings()

from robot_config import *

from flask import Flask
#Configuration().verify_ssl = False
print("(III) imports done.")


# ====================== API Auth Routines

# MIT License of Docusign on this
def create_api_client(base_path, access_token):
    """Create api client and construct API headers"""
    api_client = ApiClient()
    api_client.host = base_path
    api_client.set_default_header(header_name="Authorization", header_value=f"Bearer {access_token}")

    return api_client

def get_auth_url_parameter(prod=False):
    """
        This creates teh URL the user has to go to in order to tell their
        docusign account to allow this application.

        Using the implicit grant:
        https://developers.docusign.com/platform/auth/implicit/implicit-get-token/

        return: an url encoded URL address
    """
    dev_url = "https://account-d.docusign.com/oauth/auth?response_type=token"
    prod_url = "https://account.docusign.com/oauth/auth?response_type=token"
    base_url = dev_url
    if prod:
        base_url = prod_url

    redirect_uri = urllib.parse.quote(C_AUTH_CALLBACK, safe='')

    auth_callback = base_url \
                    + "&scope=" \
                    + C_API_SCOPE \
                    + "&client_id=" \
                    + C_API_CLIENTID \
                    + "&redirect_uri=" \
                    +  redirect_uri

    return auth_callback

def get_user_api_uri(user_token, prod=False):
    """
        Each user as a dedicated API URI (I think attached to the given token)
        It has to be collected through requesting user information with that
        given auth token (Bearer token)

    """
    if user_token == "" or user_token is None:
        return ""

    dev_url = "https://account-d.docusign.com/oauth/userinfo"
    prod_url = "https://account.docusign.com/oauth/userinfo"
    base_url = dev_url
    if prod:
        base_url = prod_url
    response = requests.get(base_url, headers={"Authorization": "Bearer "+user_token})
    return response
    
# ====================== API work routine

# The Envelopes::listStatusChanges method has many options
# See https://developers.docusign.com/esign-rest-api/reference/Envelopes/Envelopes/listStatusChanges
def get_envelopes_status(e_api):
    # The list status changes call requires at least a from_date OR
    # a set of envelopeIds. Here we filter using a from_date.
    # Here we set the from_date to filter envelopes for the last month
    # Use ISO 8601 date format
    # 1. Call the envelope status change method to list the envelopes
    from_date = (datetime.utcnow() - timedelta(days=C_WATCHED_DAYS)).isoformat()
    from_date = str(from_date).split("T")[0]
    print(from_date)
    results = e_api.list_status_changes(account_id=C_API_ACCOUNTID, from_date=from_date, _preload_content=False)
    #results = e_api.list_status(account_id=API_ACCOUNTID)
    #results = e_api.list_status_changes(account_id=API_ACCOUNTID, from_date=from_date)
    
    print("(III) ----- listing done.")
    
    print(results)
    return results


# ==================================== MAIN ===================================
if __name__ == "__main__":

    # --- local callback server
    app = Flask(__name__)

    @app.route("/")
    def hello_world():
        authorize_that_bot_url = get_auth_url_parameter(C_PROD)
        return "<p>Hello, please follow the link below to allow the robot to
        access your envelopes information</p><br/>
        <a href=\""+get_auth_url_parameter(C_PROD)+"\">Authorize Docusign
        Bot</a><br>This Robot will check statuses of your envelope and download
        the completed envelopes"

    @app.route("/ds/callback")
    def docusign_user_is_ok():
        # get the token from URL parameters
    def bot_action():
        api_client = create_api_client(base_path=C_BASE_URI, access_token="")
        print("(III) create api client done.")

        envelope_api = EnvelopesApi(api_client)
        print("(III) envelope api done.")

        get_envelopes_status(envelope_api)

